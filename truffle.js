module.exports = {
  networks: {
    development: {
      host: "localhost",
      port: 8545,
      network_id: "default" 
    },
    live: {
	network_id: 1,
	port: 8546,
	host: "localhost",
	gasPrice: 100000000,
	gas: 1000000
    },
    ropsten: {
	host: "localhost",
	port: 8547,
	network_id: "3"
    }
  }
};
