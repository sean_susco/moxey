pragma solidity ^0.4.4;

contract TradeAuthTest {
	mapping (address => uint) AccountBalances;
	address owner;

	event _FallbackTriggered(
		address Sender,
		uint WeiSent
	);

	function TradeAuthTest() public {
		owner = msg.sender;
		AccountBalances[0x7c9d3507a77C8a732ee396BA8036bE05891E3646] = 50;
		AccountBalances[0xC064a245206f780500393aF49692b9c47FeFe139] = 50;

		
	}

	function Disburse() payable public returns (bool) {
		if(msg.sender == owner){
			address a = 0x7c9d3507a77C8a732ee396BA8036bE05891E3646;
			address b = 0xC064a245206f780500393aF49692b9c47FeFe139;

			a.transfer(5000);
			b.transfer(5000);

			return true;
		}
	}

	function DestroyContract() external returns(bool){
		if(msg.sender == owner){
			selfdestruct(owner);
		}
	}

	function() payable public {
		_FallbackTriggered(msg.sender, msg.value);
	}

}
