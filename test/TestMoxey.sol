pragma solidity ^0.4.11;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/Moxey.sol";

contract TestMoxey {
	Moxey moxey = Moxey(DeployedAddresses.Moxey());


	function testPurchase() {

		uint balance = moxey.GetAccountBalance( 0x1abc88f78ff4855024cb54337decaf490706499e ); 
		Assert.equal(balance, 0, "Balance should start at 0");
		moxey.Purchase(   0x1abc88f78ff4855024cb54337decaf490706499e   ).value(10);
		
		balance = moxey.GetAccountBalance( 0x4090dedeb6bba988592a7b8c2b7d1578b4f75cd6  );
		Assert.equal(balance, 2, "Balance should be 2 after spending 10 on Purchase");
	}
}
